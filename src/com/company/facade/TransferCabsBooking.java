package com.company.facade;

public class TransferCabsBooking {

    public TransferCabsBooking() {

    }

    public void bookTransferCabs(BookingInfo info){
        System.out.println(info.getName() +" You have been successfully booked the transfer cabs package. Thank you");
    }
}
