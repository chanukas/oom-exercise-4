package com.company.facade;

public class BookingInfo {
    private String name;

    public BookingInfo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
