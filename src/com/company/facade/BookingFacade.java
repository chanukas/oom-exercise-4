package com.company.facade;

public class BookingFacade {

    private FlightBooking flightBooking;
    private TransferCabsBooking transferCabsBooking;
    private HotelBooking hotelBooking;
    private WelcomeToTicketBooking welcomeToTicketBooking;

    public BookingFacade() {
        this.welcomeToTicketBooking = new WelcomeToTicketBooking();
        this.flightBooking = new FlightBooking();
        this.transferCabsBooking = new TransferCabsBooking();
        this.hotelBooking = new HotelBooking();
    }

    public void book(BookingType type, BookingInfo info) {
        switch (type) {
            case FLIGHT:
                // book flight
                flightBooking.bookFlight(info);
                break;
            case CAB:
                //book cab
                transferCabsBooking.bookTransferCabs(info);
                break;
            case HOTEL:
                //book hotel
                hotelBooking.bookHotel(info);
                break;
        }
    }
}
