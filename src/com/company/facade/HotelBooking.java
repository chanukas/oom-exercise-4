package com.company.facade;

public class HotelBooking {

    public HotelBooking() {
    }

    public void bookHotel(BookingInfo info){
        System.out.println(info.getName() +" You have been successfully booked the hotel package. Thank you");
    }
}
