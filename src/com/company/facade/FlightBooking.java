package com.company.facade;

public class FlightBooking {
    public FlightBooking() {
    }

    public void bookFlight(BookingInfo info){
        System.out.println(info.getName() +" You have been successfully booked the flight package. Thank you");
    }
}
