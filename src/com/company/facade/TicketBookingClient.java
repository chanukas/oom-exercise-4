package com.company.facade;

public class TicketBookingClient {

    public static void main(String[] args) {

        BookingFacade bookingFacade = new BookingFacade();

        bookingFacade.book(BookingType.HOTEL,new BookingInfo("Chanuka"));

        bookingFacade.book(BookingType.CAB,new BookingInfo("Kasun"));

        bookingFacade.book(BookingType.FLIGHT,new BookingInfo("Vidura"));

    }
}
