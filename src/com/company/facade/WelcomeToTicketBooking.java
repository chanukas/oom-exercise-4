package com.company.facade;

public class WelcomeToTicketBooking {

    public WelcomeToTicketBooking() {
        System.out.println("Welcome to ticket booking system!");
        System.out.println("We are happy to provide you a better service always.");
    }
}
