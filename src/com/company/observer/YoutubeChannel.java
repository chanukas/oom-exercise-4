package com.company.observer;

import java.util.ArrayList;
import java.util.List;

public class YoutubeChannel {

    List<Subscriber> subscriberList;
    List<String> videoTitles;

   public YoutubeChannel() {
        subscriberList  = new ArrayList<>();
        videoTitles = new ArrayList<>();
    }
    public void attachSubscribers(Subscriber subscriber){
        subscriberList.add(subscriber);
    }

    public void addVideo(String title){
        videoTitles.add(title);
        //broadcast to all subscribers
        broadcastToSubscribers();
    }

    public void broadcastToSubscribers(){
        for (Subscriber sub :
                subscriberList) {
            sub.getUpdate();
        }
    }
    public List<String> getVideoTitles(){
        return videoTitles;
    }
}
