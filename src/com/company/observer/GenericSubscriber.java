package com.company.observer;


import java.util.Arrays;
import java.util.List;

public class GenericSubscriber implements Subscriber {

    YoutubeChannel youTubeChannel;
    String subscriberName;

    public GenericSubscriber(YoutubeChannel youTubeChannel,String name) {
        this.youTubeChannel = youTubeChannel;
        this.youTubeChannel.attachSubscribers(this);
        this.subscriberName = name;

    }

    @Override
    public void getUpdate() {
        List<String> videoTitles = this.youTubeChannel.getVideoTitles();
        System.out.println("Subscriber Name "+subscriberName+" "+this.getClass().getSimpleName()+" :> "+ Arrays.toString(videoTitles.toArray()));
    }
}
