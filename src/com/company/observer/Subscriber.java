package com.company.observer;

public interface Subscriber {
    void getUpdate();
}
