package com.company.observer;


public class YouTube {

    public static void main(String[] args) {
        YoutubeChannel youTubeChannel = new YoutubeChannel();
        new GenericSubscriber(youTubeChannel, "Chanuka");
        new GenericSubscriber(youTubeChannel, "Vidura");


        youTubeChannel.addVideo("Starship");
        youTubeChannel.addVideo("GOT");

        try {
            Thread.sleep(9000);
            System.out.println("Notification sent to all subscribers!");
            youTubeChannel.broadcastToSubscribers();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
