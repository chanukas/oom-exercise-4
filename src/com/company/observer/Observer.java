package com.company.observer;

public abstract class Observer {

    protected YoutubeChannel youtubeChannel;
    public abstract void update();
}
