package com.company.decorete;

public class CoffeeMachine {

    public static void main(String[] args) {

        Beverage beverage = new Sugar(new Coffee("Cappuccino"));
        beverage.decorateBeverage();

        beverage = new Vanilla(new Sugar(new Coffee("Affogato")));
        beverage.decorateBeverage();

    }
}
