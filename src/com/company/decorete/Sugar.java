package com.company.decorete;

public class Sugar extends BeverageDecorator {

    public Sugar(Beverage beverage){
        super(beverage);
    }

    @Override
    public void decorateBeverage(){
        super.decorateBeverage();
        decorateSugar();
    }

    public void decorateSugar(){
        System.out.println("Added Sugar to:"+beverage.getName());
    }

    @Override
    public int getIncrementPrice(){
        return 5;
    }

    @Override
    public String getDecoratedName(){
        return "Sugar";
    }
}
