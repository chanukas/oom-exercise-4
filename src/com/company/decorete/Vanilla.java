package com.company.decorete;

public class Vanilla extends BeverageDecorator{

    public Vanilla(Beverage beverage){
        super(beverage);
    }

    @Override
    public int getIncrementPrice() {
        return 10;
    }

    @Override
    public String getDecoratedName() {
        return "Vanilla";
    }

    @Override
    public void decorateBeverage(){
        super.decorateBeverage();
        decorateVanilla();
    }

    public void decorateVanilla(){
        System.out.println("Added Vanilla to:"+beverage.getName());
    }
}
