package com.company.decorete;

public class Coffee extends Beverage{

    public Coffee(String name){
        super(name);
        setPrice(15);
    }
    @Override
    public void decorateBeverage(){
        System.out.println("Cost of:"+ name +":"+ price);
    }
}
